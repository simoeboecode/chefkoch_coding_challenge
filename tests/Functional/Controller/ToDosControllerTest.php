<?php

namespace App\Tests\Functional\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Enum\Priority;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \App\Controller\ToDosController
 */
class ToDosControllerTest extends WebTestCase
{
    public function testIndexWithOneTaskToVerifyTheCorrectResponseFormat(): void
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1", null, new DateTimeImmutable());

        $client->loginUser($user1);

        $client->request('GET', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(1, $content);
        $returnedTask = $content[0];
        $this->assertSame($task1->getId(), $returnedTask['id']);
        $this->assertArrayNotHasKey('user', $returnedTask);
        $this->assertSame($task1->getName(), $returnedTask['name']);
        $this->assertSame($task1->getDescription(), $returnedTask['description']);
        $this->assertSame($task1->getDeadline()->format('Y-m-d H:i:s'), $returnedTask['deadline']);
        $this->assertSame($task1->getPriority(), $returnedTask['priority']);
        $this->assertSame(array_map(function ($child) { return $child->getId(); }, $task1->getChildren()->toArray()), $returnedTask['children']);
        $this->assertSame($task1->getParent(), $returnedTask['parent']);
    }

    public function testIndexWithThreeTasksOfTwoUsersToGetAllTasksOnlyOfLoggedInUser(): void
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $user2 = $this->createUser('test2@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");
        $task2 = $this->createTaskForUser($user1, "Task 2");
        $task3 = $this->createTaskForUser($user2, "Task 3");

        $client->loginUser($user1);

       $client->request('GET', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(2, $content);
        $this->assertSame($task1->getId(), $content[0]['id']);
        $this->assertSame($task2->getId(), $content[1]['id']);
    }

    public function testIndexWithThreeTasksWithoutSortingWillReturnTasksSortedById()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1", null, new DateTimeImmutable('2022-06-01 12:00:00'), Priority::NORMAL);
        $task2 = $this->createTaskForUser($user1, "Task 2", null, new DateTimeImmutable('2022-05-01 12:00:00'), Priority::HIGH);
        $task3 = $this->createTaskForUser($user1, "Task 3", null, new DateTimeImmutable('2022-07-01 12:00:00'), Priority::LOW);

        $client->loginUser($user1);

        $client->request('GET', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(3, $content);
        $this->assertSame($task1->getId(), $content[0]['id']);
        $this->assertSame($task2->getId(), $content[1]['id']);
        $this->assertSame($task3->getId(), $content[2]['id']);
    }

    public function testIndexWithThreeTasksWithSortingByDeadlineWillReturnTasksSortedByDeadline()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1", null, new DateTimeImmutable('2022-06-01 12:00:00'), Priority::NORMAL);
        $task2 = $this->createTaskForUser($user1, "Task 2", null, new DateTimeImmutable('2022-05-01 12:00:00'), Priority::HIGH);
        $task3 = $this->createTaskForUser($user1, "Task 3", null, new DateTimeImmutable('2022-07-01 12:00:00'), Priority::LOW);

        $client->loginUser($user1);

        $client->request('GET', '/todos', ['sortByDeadline' => 'ASC'], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(3, $content);
        $this->assertSame($task2->getId(), $content[0]['id']);
        $this->assertSame($task1->getId(), $content[1]['id']);
        $this->assertSame($task3->getId(), $content[2]['id']);

        $client->request('GET', '/todos', ['sortByDeadline' => 'DESC'], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(3, $content);
        $this->assertSame($task3->getId(), $content[0]['id']);
        $this->assertSame($task1->getId(), $content[1]['id']);
        $this->assertSame($task2->getId(), $content[2]['id']);
    }

    public function testIndexWithThreeTasksWithSortingByPriorityWillReturnTasksSortedByPriority()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1", null, new DateTimeImmutable('2022-06-01 12:00:00'), Priority::NORMAL);
        $task2 = $this->createTaskForUser($user1, "Task 2", null, new DateTimeImmutable('2022-05-01 12:00:00'), Priority::HIGH);
        $task3 = $this->createTaskForUser($user1, "Task 3", null, new DateTimeImmutable('2022-07-01 12:00:00'), Priority::LOW);

        $client->loginUser($user1);

        $client->request('GET', '/todos', ['sortByPriority' => 'ASC'], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(3, $content);
        $this->assertSame($task3->getId(), $content[0]['id']);
        $this->assertSame($task1->getId(), $content[1]['id']);
        $this->assertSame($task2->getId(), $content[2]['id']);

        $client->request('GET', '/todos', ['sortByPriority' => 'DESC'], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(3, $content);
        $this->assertSame($task2->getId(), $content[0]['id']);
        $this->assertSame($task1->getId(), $content[1]['id']);
        $this->assertSame($task3->getId(), $content[2]['id']);
    }

    public function testIndexWithThreeTasksWithSortingByPriorityAndDeadlineWillReturnTasksSortedByPriorityAndDeadline()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1", null, new DateTimeImmutable('2022-06-01 12:00:00'), Priority::NORMAL);
        $task2 = $this->createTaskForUser($user1, "Task 2", null, new DateTimeImmutable('2022-09-01 12:00:00'), Priority::HIGH);
        $task3 = $this->createTaskForUser($user1, "Task 3", null, new DateTimeImmutable('2022-03-01 12:00:00'), Priority::HIGH);
        $task4 = $this->createTaskForUser($user1, "Task 4", null, new DateTimeImmutable('2022-07-01 12:00:00'), Priority::LOW);

        $client->loginUser($user1);

        $client->request('GET', '/todos', ['sortByPriority' => 'ASC', 'sortByDeadline' => 'DESC'], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(4, $content);
        $this->assertSame($task4->getId(), $content[0]['id']);
        $this->assertSame($task1->getId(), $content[1]['id']);
        $this->assertSame($task2->getId(), $content[2]['id']);
        $this->assertSame($task3->getId(), $content[3]['id']);

        $client->request('GET', '/todos', ['sortByPriority' => 'DESC', 'sortByDeadline' => 'ASC'], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(4, $content);
        $this->assertSame($task3->getId(), $content[0]['id']);
        $this->assertSame($task2->getId(), $content[1]['id']);
        $this->assertSame($task1->getId(), $content[2]['id']);
        $this->assertSame($task4->getId(), $content[3]['id']);
    }

    public function testIndexWithParentAndChildTasks()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");
        $task2 = $this->createTaskForUser($user1, "Task 2", null, null, null, $task1);
        $task3 = $this->createTaskForUser($user1, "Task 3", null, null, null, $task1);

        $client->loginUser($user1);

        $client->request('GET', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing user to check if he is connected with a new task
        $em->refresh($task1);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(3, $content);
        $this->assertCount(2, $task1->getChildren());
        $this->assertSame($task1->getId(), $content[1]['parent']);
        $this->assertSame($task1->getId(), $content[2]['parent']);
    }

    public function testNewWithTaskWithAllPropertiesWillReturnTheNewTask()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $client->loginUser($user1);

        $client->request('POST', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'name' => 'Task 1',
            'description' => 'A test task',
            'deadline' => '2022-06-23 12:40:12',
            'priority' => Priority::HIGH
        ]));
        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing user to check if he is connected with a new task
        $em->refresh($user1);

        $this->assertSame(201, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $user1->getTasks());
        $content = json_decode($client->getResponse()->getContent(), true);
        /** @var Task $usersTask */
        $usersTask = $user1->getTasks()->first();
        $this->assertSame($usersTask->getId(), $content['id']);
        $this->assertSame('Task 1', $usersTask->getName());
        $this->assertSame('A test task', $usersTask->getDescription());
        $this->assertSame('2022-06-23 12:40:12', $usersTask->getDeadline()->format('Y-m-d H:i:s'));
        $this->assertSame(Priority::HIGH->value, $usersTask->getPriority()->value);
    }

    public function testNewWithTaskWithMinimumPropertiesWillReturnTheNewTask()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $client->loginUser($user1);

        $client->request('POST', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'name' => 'Task 1'
        ]));
        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing user to check if he is connected with a new task
        $em->refresh($user1);

        $this->assertSame(201, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $user1->getTasks());
        $content = json_decode($client->getResponse()->getContent(), true);
        /** @var Task $usersTask */
        $usersTask = $user1->getTasks()->first();
        $this->assertSame($usersTask->getId(), $content['id']);
        $this->assertSame('Task 1', $usersTask->getName());
        $this->assertEmpty($usersTask->getDescription());
        $this->assertEmpty($usersTask->getDeadline());
        $this->assertEmpty($usersTask->getPriority());
    }

    public function testNewWithTaskWithoutNameParameterWillReturnAn400Error()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $client->loginUser($user1);

        $client->request('POST', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'description' => 'Task 1'
        ]));
        $this->assertSame(400, $client->getResponse()->getStatusCode());
    }

    public function testNewWithAsChildTask()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");

        $client->loginUser($user1);

        $client->request('POST', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'name' => 'Task 2',
            'parent' => $task1->getId()
        ]));

        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing user to check if he is connected with a new task
        $em->refresh($task1);

        $this->assertSame(201, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $task1->getChildren());
    }

    public function testNewWithAsChildTaskOfChildShouldThrowException()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");
        $task2 = $this->createTaskForUser($user1, "Task 1", null, null, null, $task1);

        $client->loginUser($user1);

        $client->request('POST', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'name' => 'Task 3',
            'parent' => $task2->getId()
        ]));

        $this->assertSame(400, $client->getResponse()->getStatusCode());
    }

    public function testNewWithAsChildTaskOfUnknownParentShouldThrowException()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');

        $client->loginUser($user1);

        $client->request('POST', '/todos', [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'name' => 'Task 1',
            'parent' => 1234
        ]));

        $this->assertSame(400, $client->getResponse()->getStatusCode());
    }

    public function testShowWithTaskOfUserWillReturnThisTask()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");

        $client->loginUser($user1);

        $client->request('GET', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame($task1->getId(), $content['id']);
        $this->assertSame($task1->getName(), $content['name']);
    }

    public function testShowWithUnknownTaskShouldReturnNotFoundError()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');

        $client->loginUser($user1);

        $client->request('GET', '/todos/' . 1234, [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function testShowWithTaskOfOtherUserShouldReturnNotAllowedException()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $user2 = $this->createUser('test2@user.com', 'password');
        $task1 = $this->createTaskForUser($user2, "Task 1");

        $client->loginUser($user1);

        $client->request('GET', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(403, $client->getResponse()->getStatusCode());
    }

    public function testShowWithParentAndChildTasks()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");
        $task2 = $this->createTaskForUser($user1, "Task 2", null, null, null, $task1);
        $task3 = $this->createTaskForUser($user1, "Task 3", null, null, null, $task1);

        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing user to check if he is connected with a new task
        $em->refresh($task1);

        $client->loginUser($user1);

        $client->request('GET', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertCount(2, $task1->getChildren());

        $client->request('GET', '/todos/' . $task2->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertSame($task1->getId(), $content['parent']);
    }

    public function testEditWithTaskOfUserWillSaveChangedParameters()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");

        $client->loginUser($user1);

        $client->request('PUT', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'name' => 'Changed Task 1',
            'description' => 'A test task',
            'deadline' => '2022-06-23 12:40:12',
            'priority' => Priority::HIGH,
            'ready' => true
        ]));

        $this->assertSame(204, $client->getResponse()->getStatusCode());

        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing task to check the new data
        $em->refresh($task1);

        $this->assertSame('Changed Task 1', $task1->getName());
        $this->assertSame('A test task', $task1->getDescription());
        $this->assertSame('2022-06-23 12:40:12', $task1->getDeadline()->format('Y-m-d H:i:s'));
        $this->assertSame(Priority::HIGH->value, $task1->getPriority()->value);
        $this->assertTrue($task1->isReady());
    }

    public function testEditOfTaskWithSettingNameNullShouldResponseAnError()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");

        $client->loginUser($user1);

        $client->request('PUT', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ], json_encode([
            'name' => '',
            'description' => 'A test task',
            'deadline' => '2022-06-23 12:40:12',
            'priority' => Priority::HIGH
        ]));

        $this->assertSame(400, $client->getResponse()->getStatusCode());

        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing task to check the new data
        $em->refresh($task1);

        $this->assertSame('Task 1', $task1->getName());
        $this->assertEmpty($task1->getDescription());
    }

    public function testEditOfUnknownTaskShouldReturnNotFoundError()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');

        $client->loginUser($user1);

        $client->request('PUT', '/todos/' . 1234, [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function testEditOfTaskFromOtherUserShouldReturnNotAllowedException()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $user2 = $this->createUser('test2@user.com', 'password');
        $task1 = $this->createTaskForUser($user2, "Task 1");

        $client->loginUser($user1);

        $client->request('PUT', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(403, $client->getResponse()->getStatusCode());
    }

    public function testDeleteOfTaskFromUser()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $task1 = $this->createTaskForUser($user1, "Task 1");

        $client->loginUser($user1);

        $client->request('DELETE', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(204, $client->getResponse()->getStatusCode());

        /** @var EntityManagerInterface $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        // refresh existing user to check if he has no tasks anymore
        $em->refresh($user1);

        $this->assertEmpty($user1->getTasks());
    }

    public function testDeleteOfUnknownTaskShouldReturnNotFoundError()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');

        $client->loginUser($user1);

        $client->request('DELETE', '/todos/' . 1234, [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }

    public function testDeleteOfTaskFromOtherUserShouldReturnNotAllowedException()
    {
        $client = self::createClient();

        $user1 = $this->createUser('test@user.com', 'password');
        $user2 = $this->createUser('test2@user.com', 'password');
        $task1 = $this->createTaskForUser($user2, "Task 1");

        $client->loginUser($user1);

        $client->request('DELETE', '/todos/' . $task1->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);

        $this->assertSame(403, $client->getResponse()->getStatusCode());
    }

    private function createUser(string $email, string $password): User
    {
        $user = new User();
        $user->setEmail($email);

        $passwordHasher = static::getContainer()->get('security.user_password_hasher');
        $user->setPassword($passwordHasher->hashPassword($user, $password));

        $em = static::getContainer()->get('doctrine')->getManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    private function createTaskForUser(
        User $user,
        string $name,
        ?string $description = null,
        ?DateTimeImmutable $deadline = null,
        ?Priority $priority = null,
        ?Task $parentTask = null,
        array $childTasks = []
    ): Task
    {
        $task = new Task();
        $task->setName($name)
            ->setUser($user)
            ->setDescription($description)
            ->setDeadline($deadline)
            ->setPriority($priority);

        if($parentTask) {
            $task->setParent($parentTask);
        }

        foreach ($childTasks as $childTask) {
            $task->addSubTask($childTask);
        }

        $em = static::getContainer()->get('doctrine')->getManager();
        $em->persist($task);
        $em->flush();

        return $task;
    }
}
