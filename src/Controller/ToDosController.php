<?php

namespace App\Controller;

use App\Entity\Task;
use App\Enum\Priority;
use App\Repository\TaskRepository;
use App\Security\TaskVoter;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[IsGranted('ROLE_USER')]
class ToDosController extends AbstractController
{
    public function __construct(
        private readonly TaskRepository $taskRepository
    ){}

    #[Route('/todos', name: 'todos_index', methods: ['GET', 'HEAD'])]
    public function index(Request $request): JsonResponse
    {
        $sortByDeadline = $request->query->get('sortByDeadline');
        $sortByPriority = $request->query->get('sortByPriority');

        $sortBy = [];
        if($sortByPriority) {
            $sortBy['priority'] = $sortByPriority;
        }
        if($sortByDeadline) {
            $sortBy['deadline'] = $sortByDeadline;
        }

        $tasks = $this->taskRepository->findBy(['user' => $this->getUser()], $sortBy);

        $data = [];
        foreach ($tasks as $task) {
            $data[] = [
                'id' => $task->getId(),
                'name' => $task->getName(),
                'description' => $task->getDescription(),
                'deadline' => $task->getDeadline()?->format('Y-m-d H:i:s'),
                'priority' => $task->getPriority()?->value,
                'ready' => $task->isReady(),
                'parent' => $task->getParent()?->getId(),
                'children' => array_map(function ($child) { return $child->getId(); }, $task->getChildren()->toArray())
            ];
        }

        return new JsonResponse($data);
    }

    #[Route('/todos', name: 'todos_new', methods: ['POST'])]
    public function new(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $content = json_decode($request->getContent(), true);

        if(!empty($content['parent'])) {
            $parentTask = $this->taskRepository->find($content['parent']);
            if($parentTask === null) {
                return new JsonResponse("Parent Task does not exists", Response::HTTP_BAD_REQUEST);
            }
            if($parentTask->hasParent()) {
                return new JsonResponse("Child tasks can't be parents of other tasks", Response::HTTP_BAD_REQUEST);
            }
        }

        $task = new Task();

        $deadline = (!empty($content['deadline'])) ? new DateTimeImmutable($content['deadline']) : null;
        $priority = (!empty($content['priority'])) ? Priority::from($content['priority']) : null;
        $task
            ->setName($content['name'] ?? '') // Empty name is not allowed, but maybe the parameter is missing. This will return as error during the validation below. The ?? only prevent from undefined array key exception.
            ->setDescription($content['description'] ?? null)
            ->setPriority($priority)
            ->setDeadline($deadline)
            ->setParent($parentTask ?? null);

        $task->setUser($this->getUser());

        $errors = $validator->validate($task);
        if(count($errors) > 0) {
            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        $this->taskRepository->add(entity: $task, flush: true);

        return new JsonResponse(['id' => $task->getId()], Response::HTTP_CREATED);
    }

    #[Route('/todos/{id}', name: 'todos_show', methods: ['GET'])]
    public function show(Task $task): JsonResponse
    {
        $this->denyAccessUnlessGranted(attribute: TaskVoter::VIEW, subject: $task);

        return new JsonResponse([
            'id' => $task->getId(),
            'name' => $task->getName(),
            'description' => $task->getDescription(),
            'deadline' => $task->getDeadline()?->format('Y-m-d H:i:s'),
            'priority' => $task->getPriority()?->value,
            'ready' => $task->isReady(),
            'parent' => $task->getParent()?->getId(),
            'children' => array_map(function ($child) { return $child->getId(); }, $task->getChildren()->toArray())
        ]);
    }

    #[Route('/todos/{id}', name: 'todos_edit', methods: ['PUT'])]
    public function edit(Request $request, Task $task, ValidatorInterface $validator, EntityManagerInterface $entityManager): JsonResponse
    {
        $this->denyAccessUnlessGranted(attribute: TaskVoter::EDIT, subject: $task);

        $content = json_decode($request->getContent(), true);

        $deadline = (!empty($content['deadline'])) ? new DateTimeImmutable($content['deadline']) : null;
        $priority = (!empty($content['priority'])) ? Priority::from($content['priority']) : null;
        $task
            ->setName($content['name'] ?? '')
            ->setDescription($content['description'] ?? null)
            ->setPriority($priority)
            ->setDeadline($deadline)
            ->setReady($content['ready'] ?? false);

        $errors = $validator->validate($task);
        if(count($errors) > 0) {
            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        $entityManager->flush();

        return new JsonResponse(data: null, status: Response::HTTP_NO_CONTENT);
    }

    #[Route('/todos/{id}', name: 'todos_delete', methods: ['DELETE'])]
    public function delete(Task $task): JsonResponse
    {
        $this->denyAccessUnlessGranted(TaskVoter::DELETE, $task);

        $this->taskRepository->remove(entity: $task, flush: true);

        return new JsonResponse(data: null, status: Response::HTTP_NO_CONTENT);
    }
}
