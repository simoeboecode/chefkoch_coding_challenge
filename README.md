# Coding Challenge

This project contains my solution to the coding challenge.

The challenge is to create an API of a todo list app.

## Environment System
For development, I use ddev to have an environment system based on docker.

To start the project, please execute

```shell
ddev start
```

Afterwards you have the running project at [https://todolist.ddev.site/](https://todolist.ddev.site/)

There are some other helpful commands to handle the project with ddev:
```shell
ddev # without parameter you gets a full list of available commands
ddev ssh # to enter the webserver docker container
ddev describe # to get information about the project, including urls to the project, phpmyadmin, ...
```

For more information about ddev, please visit [https://ddev.readthedocs.io/en/stable/](https://ddev.readthedocs.io/en/stable/).

## Challenge Notes

Some notes, maybe as base for discussions, about the challenge details.

### Out of Scope / Alternative solutions

There are many things that are missing or maybe not optimal in this solution.
The reasons are less time or out of scope of the exact challenge.

#### Using the pipeline for tests and deployment
For a long living project, I would use a test pipeline to automatically run tests after each commit.

#### Using the serializer instead of building the response as array
For basic entity responses there is a beautiful serializer and deserializer in symfony.

In this project the configuration of special cases (e.g. do not expose the user, only send ids of children ...) are too time consuming for a demo project, so I used 'basic' array conversion to structure data for responses or handle requests.

#### Using api platform instead of implementing the whole controller
Because the challenge was only an api endpoint for a simple entity, the api platform is a great solution including api documentation and other features.

But I think, the target of the challenge was the coding and not the configuration of an api platform entity ;-)

#### Unit Test of the controller instead of only functional tests
Normally there should be more unit tests than functional tests. 

In this solution I very often use `$this->getUser()` inside the controller. It is not so easy to test this functionality with unit tests.
For example, I could wrap the 'findBy' method to a service and use the Security service to get the current user, or I could use the repository for this.

But because of the time frame, I decided to use only functional tests to easily check the whole api behaviour.

#### String Enum instead of Int Enum
The Priority Enum should be a string enum:
* better readable database entries and requests
* no heavy need of api documentation which number belongs to which state
* future proof for changes
* ...

But: if you use a string enum, you have to define your own orderBy functionality instead of sort by alphabet.
This isn't very easy and may not be necessary for an enum only with 3 values.

#### Using branches for every little feature
I have implemented the whole solution in one branch.

Normally I want to use branches for single steps (e.g. sorting, priority handling, parent/child structure..)

#### Provision script
For a long living project I prefer to have a provision script to (re)set up the project (e.g. composer i, running migrations, ...)

#### Linter and other quality checker
For long living projects I prefer to have a linter for code style and tools like PHPStan to check my code quality.

### Missing information

There are some decisions that I made for my own. In the 'real world' I would discuss these points before starting to coding.

#### API Format
I 
* use JSON Format
* send only IDs instead of sub objects (e.g. children, parent)
* use a hard programmed deadline format
* have no pagination
* named sub-tasks as children
* ...

All these bullets and more I want to discuss before implementing an API.

#### Implementation details
* Should an Admin see all tasks?
* Did I want to change parent after creating a task?
* Should I delete a task automatically after deadline?
* Which priorities do we have? Are they extendable and maybe shouldn't be an enum but an entity?
* Should the API be documented, e.g. swagger 
* Should I only see non-ready tasks at index? Parameter to select ready tasks? -> I have ignored ready parameter for index, but this is not an ideal solution.
* ..

